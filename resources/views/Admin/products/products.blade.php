@extends('Admin.admin_master')

@section('content')

        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Pagina Categorie
        <small>Lista delle categorie</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/Admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/Admin/Category">Categorie</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Title</h3>

            <div class="box-tools pull-right">
                <a href="{{ route('Admin.Product.create') }}"><button type="button" class="btn btn-primary">Crea nuovo Prodotto</button></a>
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table no-margin">
                    <thead>
                    <tr>
                        <td>ID</td>
                        <td>Image</td>
                        <td>Name</td>
                        <td>Options</td>
                    </tr>
                    </thead>
                    <tbody>
                    @if($products->count() > 0)
                    @foreach($products as $product)
                        <tr>
                            <td>{{ $product->id }}</td>
                            <td><img src="{{ $product->getFirstMediaUrl('images', 'adminThumb') }}"></td>
                            <td>{{ $product->name }}</td>
                            <td>
                                <div class="btn-group">
                                    <a href="{{ route('Admin.Product.edit', ['product' => $product->id]) }}">
                                    <button data-original-title="edit" type="button" class="btn btn-info btn-sm" data-widget="edita" data-toggle="tooltip" title="">
                                        <i class="fa fa-edit"></i></button></a>
                                    <button data-original-title="Remove" type="button" class="btn btn-info btn-sm" data-widget="cancella" data-toggle="tooltip" title="">
                                        <i class="fa fa-remove"></i></button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            Footer
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->

</section>
<!-- /.content -->

@endsection