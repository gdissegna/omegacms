<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{!! isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email  !!}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">GESTIONE UTENTI</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="active"><a href="{{ route('Admin.Users') }}"><i class="fa fa-user"></i> <span>Users</span></a></li>
        </ul>
        <!-- /.sidebar-menu -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">AMMINISTRAZIONE CONTENUTI</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="active"><a href="#"><i class="fa fa-link"></i> <span>Link</span></a></li>
            <li><a href="{{ route('Admin.Slider.index') }}"><i class="fa fa-link"></i> <span>Gestione slider</span></a></li>
            <li><a href="{{ route('Admin.Category.index') }}"><i class="fa fa-link"></i> <span>Gestione categorie</span></a></li>
            <li><a href="{{ route('Admin.Product.index') }}"><i class="fa fa-link"></i> <span>Gestione prodotti</span></a></li>
            <li><a href="{{ route('Admin.Article.index') }}"><i class="fa fa-link"></i> <span>Gestione news</span></a></li>
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>