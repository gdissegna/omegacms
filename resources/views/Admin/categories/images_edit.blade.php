@extends('Admin.admin_master')

@section('content')

        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Gestione Immagini Categoria
        <small>Gestisci immagini e downloads per le Categorie</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Modifica immagini Categoria</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Modifica Immagine</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip"
                        title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            @include('Admin._partials.errors')
            {!! Form::open(['route' => ['Admin.CategoryImages.update', $single_media->model_id, $single_media->id ], 'method' => 'post','class' => 'form-horizontal', 'files' => 'true']) !!}
            <div class="box-body">
                <div class="form-group">
                    <label for="Image" class="col-sm-2 control-label">Modifica File/Immagine</label>
                    <div class="col-sm-10">
                        <input name="image" id="Image" placeholder="Add Image" type="file">
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Crea</button>
            </div>
            <!-- /.box-footer -->
            {!! Form::close() !!}
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            Footer
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->

    @endsection