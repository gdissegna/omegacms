@extends('Admin.admin_master')

@section('content')

        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Gestione Immagini Categoria
        <small>Gestisci immagini e downloads per le Categorie</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Gestione immagini Categoria</li>
    </ol>
</section>
<div class="content-flash">
    @include('Admin._partials.errors')
</div>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Aggiungi un immagine</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip"
                        title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            @include('Admin._partials.errors')
            {!! Form::open(['route' => ['Admin.CategoryImages.store', $category_images->id  ], 'method' => 'post','class' => 'form-horizontal', 'files' => 'true']) !!}
            <div class="box-body">
                <div class="form-group">
                    <label for="Image" class="col-sm-2 control-label">Aggiungi Immagine</label>
                    <div class="col-sm-10">
                        <input name="image" id="Image" placeholder="Add Image" type="file">
                    </div>
                </div>
                <div class="form-group">
                    <label for="Image" class="col-sm-2 control-label">Aggiungi Immagine</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="collection">
                            @foreach($category_images->mediaLibraryCollections as $option)
                                <option value="{{ $option }}">{{ $option }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Crea</button>
            </div>
            <!-- /.box-footer -->
            {!! Form::close() !!}
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            Footer
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Lista immagini</h3>

            <div class="box-tools pull-right">

            </div>
        </div>
        <div class="box-body">
            @include('Admin._partials.flash')
            <hr>
            <div class="box-body">
                <ul class="todo-list ui-sortable">
                    @foreach($category_images->getMedia('images') as $image)
                        <li class="" style="">
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                            <img src="{{ $image->getUrl('adminThumb') }}">
                            <span class="text">{{ $image->id }}</span>
                            <span class="text">{{ $image->model_id }}</span>
                            <div class="tools">
                                <a href="{{ route('Admin.CategoryImages.edit',[$image->model_id, $image->id]) }}"><i class="fa fa-edit"></i></a>
                                <a href="{{ route('Admin.CategoryImages.delete',[$image->model_id, $image->id]) }}"><i class="fa fa-trash-o"></i></a>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            Footer
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Lista Download</h3>

            <div class="box-tools pull-right">

            </div>
        </div>
        <div class="box-body">
            @include('Admin._partials.flash')
            <hr>
            <div class="box-body">
                <ul class="todo-list ui-sortable">
                    @foreach($category_images->getMedia('downloads') as $file)
                        <li class="ui-state-default" style="">
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                            <span class="text">{{ $file->file_name }}</span>
                            <div class="tools">
                                <a href="{{ route('Admin.CategoryImages.edit',[$image->model_id, $image->id]) }}"><i class="fa fa-edit"></i></a>
                                <a href="{{ route('Admin.CategoryImages.delete',[$image->model_id, $image->id]) }}"><i class="fa fa-trash-o"></i></a>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            Footer
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->
</section>

@endsection