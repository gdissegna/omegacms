<?php
namespace app\Models\Fundation;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Translation extends Model
{
    public $timestamps = false;
    protected $guarded = ['id'];
}