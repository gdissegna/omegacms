<?php
namespace app\Models\Fundation;

use Illuminate\Database\Eloquent\Model;
use App\Models\Fundation\Traits\Presentable;
use App\Models\Fundation\Traits\HasMedia;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Dimsav\Translatable\Translatable as Translatable;
use Spatie\MediaLibrary\SortableTrait;

class BaseModel extends Model implements HasMediaConversions
{
    use HasMedia,Presentable,SortableTrait,Translatable;

    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function getTranslationModelNameDefault()
    {
        if (isset($this->translationModel)) {
            return $this->translationModel;
        }
        $classParts = explode('\\', get_class($this));
        $class = array_pop($classParts);
        return "App\\Models\\Translations\\{$class}Translation";
    }

    /**
     * Register the conversions that should be performed.
     *
     * @return array
     */
    public function registerMediaConversions()
    {
        // Perform a resize and sharpen on every collection
        $this->addMediaConversion('adminThumb')
            ->setManipulations(['w' => 50, 'h' => 50, 'sharp'=> 15])
            ->performOnCollections('*');
    }

}