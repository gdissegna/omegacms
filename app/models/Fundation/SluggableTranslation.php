<?php

namespace App\Models\Fundation;


use Cviebrock\EloquentSluggable\SluggableInterface as Sluggable;
use Cviebrock\EloquentSluggable\SluggableTrait as SluggableTrait;

class SluggableTranslation extends Translation implements Sluggable
{
    use SluggableTrait;
    public $timestamps = false;
    protected $guarded = ['id'];
}