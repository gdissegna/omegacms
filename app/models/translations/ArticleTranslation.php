<?php

namespace App\Models\translations;

use App\Models\Fundation\SluggableTranslation;


class ArticleTranslation extends SluggableTranslation
{
    protected $fillable = ['title', 'body'];

    protected $sluggable = [
        'build_from' => 'title',
        'save_to'    => 'slug',
    ];
}
