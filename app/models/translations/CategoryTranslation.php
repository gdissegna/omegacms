<?php

namespace App\Models\translations;

use App\Models\Fundation\SluggableTranslation;

class CategoryTranslation extends SluggableTranslation
{
    protected $fillable = ['name', 'description'];

    protected $sluggable = [
        'build_from' => 'name',
        'save_to'    => 'slug',
    ];


    public function Category()
    {
        return $this->belongsTo('category','category_id', 'id');
    }
}
