<?php

namespace App\Models\translations;

use Illuminate\Database\Eloquent\Model;

class OfferTranslation extends Model
{
    $timestamps = false;
}
