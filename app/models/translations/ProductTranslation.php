<?php

namespace App\Models\translations;


use App\Models\Fundation\SluggableTranslation;

class ProductTranslation extends SluggableTranslation
{
    $fillable = ['name','slug','description'];

    protected $sluggable = [
        'build_from' => 'name',
        'save_to'    => 'slug',
    ];

    public function Product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id', 'id');
    }
}