<?php

namespace App\Models;

use App\Models\Fundation\BaseModel;

class Product extends BaseModel
{
    public $mediaLibraryCollections = ['images', 'downloads'];
    public $translatedAttributes = ['name', 'description','meta_title','meta_description'];

    public function Category()
    {
       return $this->belongsTo('App\Models\Category');
    }

}
