<?php

namespace App\Models;

use app\models\Fundation\BaseModel;

class Event extends BaseModel
{
    public $mediaLibraryCollections = ['images', 'downloads'];
    public $translatedAttributes = ['name', 'description','meta_title','meta_description'];
}
