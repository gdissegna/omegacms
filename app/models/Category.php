<?php

namespace App\Models;

use App\Models\Fundation\BaseModel;


class Category extends BaseModel
{
    public $mediaLibraryCollections = ['images', 'downloads'];
    public $translatedAttributes = ['name', 'description','meta_title','meta_description'];

    public function Children()
    {
       return $this->hasMany('App\Models\Category', 'id');
    }

    public function Parent()
    {
        return $this->belongsTo('App\Models\Category', 'parent_id', 'id');
    }

    public function Products()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function scopeParents($query)
    {
        return $this->query()->where('parent_id', 0)->get();
    }

}
