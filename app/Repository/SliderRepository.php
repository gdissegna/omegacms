<?php
namespace App\Repository;

use App\Models\Slider;
use App\Http\Requests\Request;
use App\Http\Requests\SliderRequest;

class SliderRepository implements RepositoryInterface
{


    /**
     * SliderRepository constructor.
     * @param Slider $slider
     */
    public function __construct(Slider $slider)
    {
        $this->slider = $slider;
    }

    /**
     * @return mixed
     */
    public function all()
    {
        return $this->slider->with('media')->orderBy('updated_at', 'desc')->get();
    }

    /**
     * @param $perPage
     * @return mixed
     */
    public function paginate($perPage)
    {
        return $this->slider->with('images')->orderBy('updated_at', 'desc')->paginate(99);
    }

    /**
     * @param Request $data
     */
    public function create(Request $data)
    {
        $newslider = new Slider();

        foreach (\Config::get('app.locales') as $locale => $name) {
            $newslider->translateOrNew($locale)->name = $data['title_'. $locale];
            $newslider->translateOrNew($locale)->text = $data['text_'. $locale];
        }

        $newslider->save();

        $newslider->addMedia($data->file('image'))->toCollection('images');

        $newslider->save();
    }

    public function update(Request $data, $id)
    {

    }

    public function delete($id)
    {
        $slider = Slider::findOrFail($id);

        $slider->delete();
    }

    public function find($id)
    {
        // TODO: Implement find() method.
    }

    public function findBy($field, $value)
    {
        // TODO: Implement findBy() method.
    }
}