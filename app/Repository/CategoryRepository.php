<?php


namespace App\Repository;

use App\models\Category;
use App\Http\Requests\Request;

class CategoryRepository implements RepositoryInterface
{

    public $category;

    /**
     * CategoryRepository constructor.
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function All()
    {
        return $this->category->with('media')->orderBy('updated_at', 'desc')->get();
    }

    /**
     * @param int $perPage
     *
     * @return mixed
     */
    public function Paginate($perPage = 20)
    {
        return $this->category->with('media')
            ->orderBy('updated_at', 'desc')
            ->paginate($perPage);
    }

    public function create(Request $request)
    {
        $newcategory = new Category();

        foreach (config('app.locales') as $locale => $name) {
            $newcategory->translateOrNew($locale)->name = $request['name_' . $locale];
            $newcategory->translateOrNew($locale)->description = $request['description_' . $locale];
            $newcategory->translateOrNew($locale)->meta_title = $request['meta_title_' . $locale];
            $newcategory->translateOrNew($locale)->meta_description = $request['meta_description_' . $locale];
        }

        $newcategory->parent_id = $request['parent'];

        $newcategory->save();

        $newcategory->addMedia($request->file('image'))->toCollection('images');
        $newcategory->save();
    }

    public function update(Request $request, $id)
    {
        $category = $this->category->find($id);

        foreach (config('app.locales') as $locale => $name) {
            $category->translateOrNew($locale)->name = $request->has('name_' . $locale) ? $request['name_' . $locale] : $category->translateOrNew($locale)->name;
            $category->translateOrNew($locale)->description = $request->has('description_' . $locale) ? $request['description_' . $locale] : $category->translateOrNew($locale)->description;
            $category->translateOrNew($locale)->meta_title = $request->has('meta_title_' . $locale) ? $request['meta_title_' . $locale] : $category->translateOrNew($locale)->meta_title;
            $category->translateOrNew($locale)->meta_description = $request->has('meta_description_' . $locale) ? $request['meta_description_' . $locale] : $category->translateOrNew($locale)->meta_description;
        }

        $category->parent_id = request['parent'];
        $category->save();
    }

    public function delete($id)
    {
        $category = $this->category->find($id)->first();
        if ($this->category->parent === 0 and $this->category->Children()->count() > 0) {
            $childs_to_delete = $this->category->Children();
            foreach ($childs_to_delete as $child) {
                $child->delete();
            }
        }
        $category->delete();
    }

    public function find($id)
    {
        return $this->category->findOrFail($id);
    }

    public function findBy($field, $value)
    {
        return $this->category->where($field, $value)->get();
    }

    public function findBySlug($slug)
    {

    }

    /**
     * @return mixed
     */
    public function allParents()
    {
        return $this->category->Parents();
    }

    public function findMedia($category_id, $media_id)
    {
        $parent_category = $this->category->find($category_id);
        return $parent_category->getMedia()->where('id', (int)$media_id)->first();
    }

    /**
     * @param $id
     * @param Request $request
     */
    public function addMedia(Request $request, $id)
    {
        $cat_media_edit = $this->category->find($id);

        $cat_media_edit->addMedia($request['image'])->toCollection($request['collection']);
    }

    public function updateMedia(Request $request, $category_id, $media_id)
    {
        $parent_category = $this->category->find($category_id);
        $single_media = $parent_category->getMedia()->where('id', (int)$media_id)->first();

        $this->category->updateMedia($single_media, $request->file('image'));
    }

    public function deleteMedia($category_id, $media_id)
    {
        $parent_category = $this->category->find($category_id);

        $single_media = $parent_category->getMedia()->where('id', (int)$media_id)->first();

        $single_media->delete();
    }

}