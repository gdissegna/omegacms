<?php


namespace App\Repository;

use App\Repository\RepositoryInterface;
use App\Http\Requests\Request;
use App\Models\Product;

/**
 * Class ProductRepository
 * @package App\Repository
 */
class ProductRepository implements RepositoryInterface
{
    /**
     * ProductRepository constructor.
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function all()
    {
        return $this->product->with('media')->orderBy('updated_at', 'desc')->get();
    }

    /**
     * @param $perPage
     * @return mixed
     */
    public function paginate($perPage = 20)
    {
        return $this->product->with('media')->orderBy('updated_at', 'desc')->paginate($perPage);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request)
    {
        $newProduct = new Product();

        foreach (config('app.locales') as $locale => $name) {
            $newProduct->translateOrNew($locale)->name = $request['name_' . $locale];
            $newProduct->translateOrNew($locale)->description = $request['description_' . $locale];
        }

        if ($request->has('code')) {
            $newProduct->code = $request['code'];
        }

        $newProduct->save();

        $newProduct->addMedia($request->file('image'))->toCollection('images');
        $newProduct->save();

        return $newProduct;
    }

    /**
     * @param Request $data
     * @param $id
     * @return mixed
     */
    public function update(Request $data, $id)
    {
        $updateProduct = $this->product->find($id);

        foreach (config('app.locales') as $locale => $name) {
            $updateProduct->translateOrNew($locale)->name = $request['name_' . $locale];
            $updateProduct->translateOrNew($locale)->description = $request['description_' . $locale];
        }

        if ($request->has('code')) {
            $updateProduct->code = $request['code'];
        }

        return $newProduct->save();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $prod_delete = $this->find($id);

        return $prod_delete->delete();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->product->findOrFail($id);
    }

    /**
     * @param $field
     * @param $value
     * @return mixed
     */
    public function findBy($field, $value)
    {
        return $this->product->where($field, $value)->get();
    }

    public function addMedia(Request $request, $id)
    {
        $product_media = $this->product->findOrFail($id);

        $product_media->addMedia($request['file']->toCollection($request['collection']));
    }


    public function deleteMedia($product_id, $media_id)
    {
        $parent_product = $this->category->find($category_id);

        $single_media = $parent_product->getMedia()->where('id', (int)$media_id)->first();

        $single_media->delete();
    }

}