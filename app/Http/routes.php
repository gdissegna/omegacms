<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('', [ 'as' => '', 'uses' => '']);
// resource('', '');


// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

Route::group(['middleware' => 'auth', 'prefix' => 'Admin'], function () {
    // Registration routes...
    Route::get('auth/register', 'Auth\AuthController@getRegister');
    Route::post('auth/register', 'Auth\AuthController@postRegister');
    // Dashboard Routes...
    Route::get('/dashboard', ['as' => 'dashboard', 'uses' => 'Admin\DashboardController@index']);
    Route::get('/Users', ['as' => 'Admin.Users', 'uses' => 'Admin\UserController@index']);
    // get('/Users', [ 'as' => 'Admin.Users.create', 'uses' => 'Admin\UserController@create']);
    Route::post('/Users', ['as' => 'Admin.Users.store', 'uses' => 'Admin\UserController@store']);
    Route::get('/Users/{id}/destroy', ['as' => 'Admin.Users.destroy', 'uses' => 'Admin\UserController@destroy']);
    // Slider Routes
    // resource('Slider', 'Admin\SliderController');
    Route::get('/Slider', ['as' => 'Admin.Slider.index', 'uses' => 'Admin\SliderController@index']);
    Route::post('/Slider', ['as' => 'Admin.Slider.store', 'uses' => 'Admin\SliderController@store']);
    Route::get('/Slider/{slider}/delete', ['as' => 'Admin.Slider.destroy', 'uses' => 'Admin\SliderController@destroy']);
    // News Routes
    Route::get('/Articles', ['as' => 'Admin.Article.index', 'uses' => 'Admin\ArticlesController@index']);
    Route::post('/Articles', ['as' => 'Admin.Article.store', 'uses' => 'Admin\ArticlesController@store']);
    Route::get('/Articles/create', ['as' => 'Admin.Article.create', 'uses' => 'Admin\ArticlesController@create']);
    Route::get('/Articles/{article}', ['as' => 'Admin.Article.show', 'uses' => 'Admin\ArticlesController@show']);
    Route::get('/Articles/{article}/edit', ['as' => 'Admin.Article.edit', 'uses' => 'Admin\ArticlesController@edit']);
    Route::post('/Articles/{article}/update',
        ['as' => 'Admin.Article.update', 'uses' => 'Admin\ArticlesController@update']);
    Route::get('/Articles/{article}/destroy',
        ['as' => 'Admin.Article.destroy', 'uses' => 'Admin\ArticlesController@destroy']);
    Route::get('/Articles/{article}/destroy',
        ['as' => 'Admin.Article.destroy', 'uses' => 'Admin\ArticlesController@destroy']);
    // Category Routes
    // resource('Category', 'Admin\CategoriesController');
    Route::get('/Category', ['as' => 'Admin.Category.index', 'uses' => 'Admin\CategoriesController@index']);
    Route::post('/Category', ['as' => 'Admin.Category.store', 'uses' => 'Admin\CategoriesController@store']);
    Route::get('/Category/create', ['as' => 'Admin.Category.create', 'uses' => 'Admin\CategoriesController@create']);
    Route::get('/Category/{category}', ['as' => 'Admin.Category.show', 'uses' => 'Admin\CategoriesController@show']);
    Route::get('/Category/{category}/edit',
        ['as' => 'Admin.Category.edit', 'uses' => 'Admin\CategoriesController@edit']);
    Route::post('/Category/{category}/update',
        ['as' => 'Admin.Category.update', 'uses' => 'Admin\CategoriesController@update']);
    Route::get('/Category/{category}/destroy',
        ['as' => 'Admin.Category.destroy', 'uses' => 'Admin\CategoriesController@destroy']);
    Route::get('Category/{category}/Images',
        ['as' => 'Admin.CategoryImages.index', 'uses' => 'Admin\CategoriesController@imagesIndex']);
    Route::post('Category/{category}/Images',
        ['as' => 'Admin.CategoryImages.store', 'uses' => 'Admin\CategoriesController@imagesStore']);
    Route::get('Category/{category}/Images/{id}',
        ['as' => 'Admin.CategoryImages.delete', 'uses' => 'Admin\CategoriesController@imagesDelete']);
    Route::get('Category/{category}/Images/{id}/edit',
        ['as' => 'Admin.CategoryImages.edit', 'uses' => 'Admin\CategoriesController@imagesEdit']);
    Route::post('Category/{category}/Images/{id}/update',
        ['as' => 'Admin.CategoryImages.update', 'uses' => 'Admin\CategoriesController@imagesUpdate']);
    //Product Routes
    //resource('Product', 'Admin\ProductsController');
    Route::get('/Product', ['as' => 'Admin.Product.index', 'uses' => 'Admin\ProductsController@index']);
    Route::post('/Product', ['as' => 'Admin.Product.store', 'uses' => 'Admin\ProductsController@store']);
    Route::get('/Product/create', ['as' => 'Admin.Product.create', 'uses' => 'Admin\ProductsController@create']);
    Route::get('/Product/{Product}', ['as' => 'Admin.Product.show', 'uses' => 'Admin\ProductsController@show']);
    Route::get('/Product/{Product}/edit', ['as' => 'Admin.Product.edit', 'uses' => 'Admin\ProductsController@edit']);
    Route::post('/Product/{Product}/update',
        ['as' => 'Admin.Product.update', 'uses' => 'Admin\ProductsController@update']);
    Route::get('/Product/{Product}/destroy',
        ['as' => 'Admin.Product.destroy', 'uses' => 'Admin\ProductsController@destroy']);
    Route::get('Product/{Product}/Images',
        ['as' => 'Admin.ProductImages.index', 'uses' => 'Admin\ProductsController@imagesIndex']);
    Route::post('Product/{Produtc}/Images',
        ['as' => 'Admin.ProductImages.store', 'uses' => 'Admin\ProductsController@imagesStore']);
    Route::get('Product/{Product}/Images/{id}',
        ['as' => 'Admin.ProductImages.delete', 'uses' => 'Admin\CategoriesController@imagesDelete']);
    Route::get('Product/{Product}/Images/{id}/edit',
        ['as' => 'Admin.ProductImages.edit', 'uses' => 'Admin\CategoriesController@imagesEdit']);
    Route::post('Product/{Product}/Images/{id}/update',
        ['as' => 'Admin.ProductImages.update', 'uses' => 'Admin\CategoriesController@imagesUpdate']);
    //Event Routes
    Route::get('/Event', ['as' => 'Admin.Event.index', 'uses' => 'Admin\EventController@index']);
    Route::post('/Event', ['as' => 'Admin.Event.store', 'uses' => 'Admin\EventController@store']);
    Route::get('/Event/create', ['as' => 'Admin.Event.create', 'uses' => 'Admin\EventController@create']);
    Route::get('/Event/{Event}', ['as' => 'Admin.Event.show', 'uses' => 'Admin\EventController@show']);
    Route::get('/Event/{Event}/edit', ['as' => 'Admin.Event.edit', 'uses' => 'Admin\EventController@edit']);
    Route::post('/Event/{Event}/update', [ 'as' => 'Admin.Event.update', 'uses' => 'Admin\EventController@update']);
    Route::get('/Event/{Event}/destroy', [ 'as' => 'Admin.Event.ddestroy', 'uses' => 'Admin\EventController@destroy']);
});

Route::get('/', function () {
    return view('welcome');
});
