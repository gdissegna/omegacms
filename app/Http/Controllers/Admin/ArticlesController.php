<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\ArticleEditRequest;
use App\Http\Requests\Admin\ArticleCreateRequest;
use App\Http\Controllers\Controller;
use App\Repository\ArticleRepository;

class ArticlesController extends Controller
{
    public $article;

    public function __construct(ArticleRepository $article)
    {
        $this->article = $article;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = $this->article->all();

        return view('Admin.news.articles')->with('articles', $articles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleCreateRequest $request)
    {
        $this->article->create($request);

        return route('Admin.Article.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $show_article = $this->article->find($id);

        dd('route  to implement');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit_article = $this->article->find($id);

        return view('Admin.news.edit')->with('edit_article', $edit_article);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleEditRequest $request, $id)
    {
        $this->article->update($request, $id);

        return route('Admin.Article.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->article->delete($id);
    }

    public function imagesindex($id)
    {
       $article_media = $this->article->find($id);

        return view('admin.articles.images')->with('article_media', $article_media);
    }
}
