<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CreateProductImageRequest;
use App\Http\Requests\Admin\ProductRequest;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use App\Http\Controllers\Controller;
use Flash;
use Illuminate\Http\Request;
use App\Http\Requests;


class ProductsController extends Controller
{
    /**
     * ProductsController constructor.
     */
    public function __construct(ProductRepository $product , CategoryRepository $category)
    {
        $this->product = $product;
        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     * @var $product
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = $this->product->all();

        return view('admin.products.products')->with('products', $products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = $this->category->all();

        return view('admin.products.create')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ProductRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $this->product->create($request);

        return redirect()->route('Admin.Product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $single_product = $this->product->find($id);

        return view('Admin.products.products')->with('single_product', $single_product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit_product = $this->product->find($id);

        return view('Admin.products.edit')->with('edit_product', $edit_product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->product->update($request,$id);
        flash::success('prodotto aggiornato con successo');

        return redirect()->route('Admin.Product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->product->delete($id);
        flash::success('prodotto cancellato con successo');

        return redirect()->route('Admin.Product.index');
    }

    public function imagesIndex($id)
    {
        $product_images = $this->product->find($id);

        return view('admin.productImages.index')->with('product_images', $product_images);
    }

    /**
     * @param CreateProductImageRequest $request
     */
    public function imagesStore(CreateProductImageRequest $request, $id)
    {
        $this->product->addMedia($request, $id);
        Flash::success('Immagine\file aggiunto con successo');
        return redirect()->route('Admin.ProductImages.index',[$id]);
    }

    public function imagesDelete($product_id, $media_id)
    {
        $this->product->deleteMedia($product_id, $media_id);

        return redirect()->route('Admin.ProductImages.index',[$product_id]);
    }
}
