<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repository\EventRepository;

class EventController extends Controller
{
    protected $event;
    /**
     * EventController constructor.
     */
    public function __construct(EventRepository $event)
    {
        $this->event = $event;
    }

    public function index()
    {
        $events = $this->event->all();
    }

    public function create()
    {
        
    }

    public function store()
    {
    }

    public function edit()
    {
    }

    public function update()
    {
    }

    public function destroy()
    {
    }
}
