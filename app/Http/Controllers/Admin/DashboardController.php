<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Spatie\LaravelAnalytics\LaravelAnalytics;


class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $analyticsData = $LaravelAnalytics->getVisitorsAndPageViews(7);

        return view('Admin.dashboard');
    }

}
